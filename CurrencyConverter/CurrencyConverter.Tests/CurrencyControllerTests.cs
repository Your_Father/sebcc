﻿using CurrencyConverter.Data;
using CurrencyConverter.Logic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CurrencyConverter.Tests
{
    public class CurrencyControllerTests
    {
        [Fact]
        async void LoadCurrency_CurrencyShouldBeRecieved()
        {
            // Arrange

            var date = new DateTime(1999, 05, 03);
            var @base = "EUR";
            IECBCurrency expected = new ECBCurrency()
            {
                Base = @base,
                Date = date,
                Rates = new Dictionary<string, decimal>() { { "CAD", 1.5415m }, { "HKD", 8.2073m }, { "LVL", 0.6279m }, { "DKK", 7.432m }, { "HUF", 250.02m }, { "CZK", 37.507m }, { "AUD", 1.5987m }, { "SEK", 8.9185m }, { "USD", 1.0589m }, { "LTL", 4.2367m }, { "TRL", 415285.0m }, { "JPY", 126.93m }, { "SKK", 44.896m }, { "CYP", 0.57867m }, { "CHF", 1.6111m }, { "SGD", 1.8015m }, { "ROL", 15806.0m }, { "SIT", 193.505m }, { "MTL", 0.4253m }, { "NOK", 8.2445m }, { "NZD", 1.8916m }, { "ZAR", 6.4308m }, { "ISK", 78.18m }, { "EEK", 15.6466m }, { "GBP", 0.6571m }, { "KRW", 1260.36m }, { "PLN", 4.1763m } }
            };
            // Act
            IECBCurrency actual = await new CurrencyController().LoadCurrencyAsync<ECBCurrency>(date, @base);

            // Assert
            Assert.Equal(expected.Base, actual.Base);
            Assert.Equal(expected.Date, actual.Date);
            Assert.Equal(expected.Rates.Keys, actual.Rates.Keys);
            Assert.Equal(expected.Rates.Values, actual.Rates.Values);
        }

        [Fact]
        void ExtractAvaliableCurrincies_CurrencyNamesShouldBeExtracted()
        {
            // Arrange
            IBaseCurrency currency = new ECBCurrency()
            {
                Base = "EUR",
                Date = new DateTime(1999, 05, 03),
                Rates = new Dictionary<string, decimal>() { { "GBP", 0.6571m }, { "KRW", 1260.36m }, { "PLN", 4.1763m } }
            };
            string[] expected = { "GBP", "KRW", "PLN" };
            // Act
            string[] actual = new CurrencyController().ExtractAvaliableCurrincyNames(currency);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        void CountAmount_CurrencyStringAmountShouldBeCalculated()
        {
            // Arrange
            string amount = "10";
            string rate = "1.5";
            string expected = "15,0";
            // Act
            string actual = new CurrencyController().CountAmount(amount, rate);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        void CountAmount_CurrencyDecimalAmountShouldBeCalculated()
        {
            // Arrange
            decimal amount = 10m;
            decimal rate = 1.5m;
            string expected = "15,0";
            // Act
            string actual = new CurrencyController().CountAmount(amount, rate);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        void CountAmount_CurrencyStringDecimalAmountShouldBeCalculated()
        {
            // Arrange
            string amount = "10";
            decimal rate = 1.5m;
            string expected = "15,0";
            // Act
            string actual = new CurrencyController().CountAmount(amount, rate);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
