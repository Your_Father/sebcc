﻿using CurrencyConverter.Data;
using CurrencyConverter.Logic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CurrencyConverter.Tests
{
    public class CurrencyValidatorTests
    {
        [Fact]
        void CurrencyExists_CurrencyShouldNotReturnNull()
        {
            // Arrange
            IBaseCurrency currency = new BaseCurrency()
            {
                Date = new DateTime(1999, 05, 03),
                Rates = new Dictionary<string, decimal>() { { "GBP", 0.6571m }, { "KRW", 1260.36m }, { "PLN", 4.1763m } }
            };
            // Act
            bool actual = new CurrencyValidator().CurrencyExists(currency);

            // Assert
            Assert.True(actual);
        }

        [Fact]
        void RateIsValid_CurrencySholdNotReturnZero()
        {
            // Arrange
            string currencyName = "KRW";
            decimal rate;
            IBaseCurrency currency = new BaseCurrency()
            {
                Date = new DateTime(1999, 05, 03),
                Rates = new Dictionary<string, decimal>() { { "GBP", 0.6571m }, { "KRW", 1260.36m }, { "PLN", 4.1763m } }
            };
            // Act
            bool actual = new CurrencyValidator().RateIsValid(currency, currencyName, out rate);

            // Assert
            Assert.True(actual);
        }
    }
}
