﻿using CurrencyConverter.Data;
using CurrencyConverter.Logic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Logic.Utilities
{
    public class CurrencyController : ICurrencyController
    {

        public async Task<T> LoadCurrencyAsync<T>(DateTime date, string currencyName = "USD")
        {
            string validDate = date.ToShortDateString();
            string request = $"{validDate}?base={currencyName}";
            T currency = default(T);
            using (HttpResponseMessage response = await ApiClientSingleton.Instance.GetAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    currency = await response.Content.ReadAsAsync<T>();
                }
                return currency;
            }
        }

        public string CountAmount(string amount, decimal currentRate)
        {
            decimal parsedValue = default;
            decimal.TryParse(amount, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out parsedValue);
            return parsedValue != default ? $"{currentRate * parsedValue}" : "0";
        }

        public string CountAmount(string amount, string currentRate)
        {
            decimal parsedAmount = default;
            decimal parsedCurrentRate = default;
            decimal.TryParse(amount, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out parsedAmount);
            decimal.TryParse(currentRate, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out parsedCurrentRate);
            return (parsedAmount != default) && parsedCurrentRate != default ? $"{parsedCurrentRate * parsedAmount}" : "";
        }

        public string CountAmount(decimal amount, decimal currentRate)
        {
            return $"{currentRate * amount}";
        }

        public virtual string[] ExtractAvaliableCurrincyNames(IBaseCurrency currency)
        {
            return currency.Rates.Keys.OrderBy(n => n).ToArray<string>();
        }

    }
}
