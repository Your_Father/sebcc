﻿using CurrencyConverter.Data;
using System;
using System.Threading.Tasks;

namespace CurrencyConverter.Logic.Utilities
{
    public interface ICurrencyController
    {
        /// <summary>
        /// Gets avaliable currency names from a currency object
        /// </summary>
        /// <param name="currency"></param>
        /// <returns>Avaliable currency names</returns>
        string[] ExtractAvaliableCurrincyNames(IBaseCurrency currency);

        /// <summary>
        /// Loads currencies from the web
        /// </summary>
        /// <typeparam name="T">Currency type</typeparam>
        /// <param name="date">Date for historical currencies</param>
        /// <param name="currencyName">Currency short name</param>
        /// <returns>Loaded currency</returns>
        Task<T> LoadCurrencyAsync<T>(DateTime date, string currencyName = "USD");

        /// <summary>
        /// Calculates the amount of converted currency 
        /// </summary>
        /// <param name="amount">Amount of initial currency</param>
        /// <param name="currentRate">Currency rate</param>
        /// <returns>Calculated the amount of converted currency</returns>
        string CountAmount(string amount, decimal currentRate);

        /// <summary>
        /// Calculates the amount of converted currency 
        /// </summary>
        /// <param name="amount">Amount of initial currency</param>
        /// <param name="currentRate">Currency rate</param>
        /// <returns>Calculated the amount of converted currency</returns>
        string CountAmount(string amount, string currentRate);

        /// <summary>
        /// Calculates the amount of converted currency 
        /// </summary>
        /// <param name="amount">Amount of initial currency</param>
        /// <param name="currentRate">Currency rate</param>
        /// <returns>Calculated the amount of converted currency</returns>
        string CountAmount(decimal amount, decimal currentRate);
    }
}