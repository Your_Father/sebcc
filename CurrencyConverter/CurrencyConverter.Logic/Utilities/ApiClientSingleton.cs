﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CurrencyConverter.Logic
{
    public class ApiClientSingleton : HttpClient
    {
        private ApiClientSingleton()
        {
            BaseAddress = new Uri("https://api.exchangeratesapi.io/");
            DefaultRequestHeaders.Accept.Clear();
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static ApiClientSingleton instance = null;
        public static ApiClientSingleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApiClientSingleton();
                }
                return instance;
            }
        }
    }
}
