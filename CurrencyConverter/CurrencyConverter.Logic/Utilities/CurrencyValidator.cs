﻿using CurrencyConverter.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Logic.Utilities
{
    public class CurrencyValidator : ICurrencyValidator
    {

        public virtual bool CurrencyExists(IBaseCurrency currency)
        {
            return currency != null;
        }


        public virtual bool RateIsValid(IBaseCurrency currency, string currencyName, out decimal rate)
        {
            rate = currency.Rates.
                            Where(r => r.Key == currencyName).
                            Select(r => r.Value).FirstOrDefault();
            return rate >= 0;
        }
    }
}
