﻿using CurrencyConverter.Data;

namespace CurrencyConverter.Logic.Utilities
{
    public interface ICurrencyValidator
    {

        /// <summary>
        /// Checks if the currency is null
        /// </summary>
        /// <param name="currency">currency object</param>
        /// <returns>True if exists, otherwise false</returns>
        bool CurrencyExists(IBaseCurrency currency);

        /// <summary>
        /// Checks weather the rate exists
        /// </summary>
        /// <param name="currency">'From' currency object</param>
        /// <param name="currencyName">Currency name to which we want convert</param>
        /// <param name="rate">Rate variablie</param>
        /// <returns>True if exists, otherwise false</returns>
        bool RateIsValid(IBaseCurrency currency, string currencyName, out decimal rate);
    }
}