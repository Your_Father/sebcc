﻿namespace CurrencyConverter.Data
{
    public interface IECBCurrency : IBaseCurrency
    {
        string Base { get; set; }
    }
}