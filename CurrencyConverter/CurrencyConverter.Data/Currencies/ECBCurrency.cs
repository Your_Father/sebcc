﻿namespace CurrencyConverter.Data
{
    public class ECBCurrency : BaseCurrency, IECBCurrency
    {
        public string Base { get; set ; }
    }
}
