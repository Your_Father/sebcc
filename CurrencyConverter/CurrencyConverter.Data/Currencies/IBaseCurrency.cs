﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Data
{
    public interface IBaseCurrency
    {
        Dictionary<string, decimal> Rates { get; set; }
        DateTime Date { get; set; }
    }
}