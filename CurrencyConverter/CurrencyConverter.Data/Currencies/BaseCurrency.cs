﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyConverter.Data
{
    public class BaseCurrency : IBaseCurrency
    {
        public Dictionary<string, decimal> Rates { get; set; }
        public DateTime Date { get; set; }
    }
}
