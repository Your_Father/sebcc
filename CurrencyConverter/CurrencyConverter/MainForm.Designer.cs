﻿

namespace CurrencyConverter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.currencyMain_comboBox = new System.Windows.Forms.ComboBox();
            this.currencyComparable_comboBox = new System.Windows.Forms.ComboBox();
            this.rate_label = new System.Windows.Forms.Label();
            this.rate_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.amount_textBox = new System.Windows.Forms.TextBox();
            this.amount_label = new System.Windows.Forms.Label();
            this.from_label = new System.Windows.Forms.Label();
            this.to_label = new System.Windows.Forms.Label();
            this.date_checkBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // currencyMain_comboBox
            // 
            this.currencyMain_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyMain_comboBox.FormattingEnabled = true;
            this.currencyMain_comboBox.Location = new System.Drawing.Point(64, 20);
            this.currencyMain_comboBox.Name = "currencyMain_comboBox";
            this.currencyMain_comboBox.Size = new System.Drawing.Size(142, 21);
            this.currencyMain_comboBox.TabIndex = 0;
            this.currencyMain_comboBox.SelectedIndexChanged += new System.EventHandler(this.currency_control_RelevantDataChanged);
            this.currencyMain_comboBox.SelectedValueChanged += new System.EventHandler(this.currency_comboBox_SelectedValueChanged);
            this.currencyMain_comboBox.Enter += new System.EventHandler(this.currencyMain_comboBox_Enter);
            // 
            // currencyComparable_comboBox
            // 
            this.currencyComparable_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyComparable_comboBox.FormattingEnabled = true;
            this.currencyComparable_comboBox.Location = new System.Drawing.Point(64, 57);
            this.currencyComparable_comboBox.Name = "currencyComparable_comboBox";
            this.currencyComparable_comboBox.Size = new System.Drawing.Size(142, 21);
            this.currencyComparable_comboBox.TabIndex = 1;
            this.currencyComparable_comboBox.SelectedValueChanged += new System.EventHandler(this.currency_comboBox_SelectedValueChanged);
            // 
            // rate_label
            // 
            this.rate_label.AutoSize = true;
            this.rate_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rate_label.Location = new System.Drawing.Point(12, 97);
            this.rate_label.Name = "rate_label";
            this.rate_label.Size = new System.Drawing.Size(48, 20);
            this.rate_label.TabIndex = 2;
            this.rate_label.Text = "Rate:";
            this.rate_label.TextChanged += new System.EventHandler(this.sum_control_TextChanged);
            // 
            // rate_dateTimePicker
            // 
            this.rate_dateTimePicker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rate_dateTimePicker.CustomFormat = "";
            this.rate_dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.rate_dateTimePicker.Location = new System.Drawing.Point(16, 234);
            this.rate_dateTimePicker.MinDate = new System.DateTime(1999, 1, 4, 0, 0, 0, 0);
            this.rate_dateTimePicker.Name = "rate_dateTimePicker";
            this.rate_dateTimePicker.Size = new System.Drawing.Size(181, 20);
            this.rate_dateTimePicker.TabIndex = 3;
            this.rate_dateTimePicker.Visible = false;
            this.rate_dateTimePicker.ValueChanged += new System.EventHandler(this.currency_control_RelevantDataChanged);
            // 
            // amount_textBox
            // 
            this.amount_textBox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.amount_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.amount_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amount_textBox.Location = new System.Drawing.Point(16, 131);
            this.amount_textBox.Name = "amount_textBox";
            this.amount_textBox.Size = new System.Drawing.Size(190, 17);
            this.amount_textBox.TabIndex = 4;
            this.amount_textBox.TextChanged += new System.EventHandler(this.sum_control_TextChanged);
            this.amount_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.amount_textBox_KeyPress);
            // 
            // amount_label
            // 
            this.amount_label.AutoSize = true;
            this.amount_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amount_label.Location = new System.Drawing.Point(12, 168);
            this.amount_label.Name = "amount_label";
            this.amount_label.Size = new System.Drawing.Size(0, 24);
            this.amount_label.TabIndex = 5;
            // 
            // from_label
            // 
            this.from_label.AutoSize = true;
            this.from_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.from_label.Location = new System.Drawing.Point(12, 21);
            this.from_label.Name = "from_label";
            this.from_label.Size = new System.Drawing.Size(46, 20);
            this.from_label.TabIndex = 6;
            this.from_label.Text = "From";
            // 
            // to_label
            // 
            this.to_label.AutoSize = true;
            this.to_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.to_label.Location = new System.Drawing.Point(12, 58);
            this.to_label.Name = "to_label";
            this.to_label.Size = new System.Drawing.Size(27, 20);
            this.to_label.TabIndex = 7;
            this.to_label.Text = "To";
            // 
            // date_checkBox
            // 
            this.date_checkBox.AutoSize = true;
            this.date_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_checkBox.Location = new System.Drawing.Point(16, 204);
            this.date_checkBox.Name = "date_checkBox";
            this.date_checkBox.Size = new System.Drawing.Size(138, 24);
            this.date_checkBox.TabIndex = 8;
            this.date_checkBox.Text = "On special date";
            this.date_checkBox.UseVisualStyleBackColor = true;
            this.date_checkBox.CheckedChanged += new System.EventHandler(this.date_checkBox_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(218, 286);
            this.Controls.Add(this.date_checkBox);
            this.Controls.Add(this.to_label);
            this.Controls.Add(this.from_label);
            this.Controls.Add(this.amount_label);
            this.Controls.Add(this.amount_textBox);
            this.Controls.Add(this.rate_dateTimePicker);
            this.Controls.Add(this.rate_label);
            this.Controls.Add(this.currencyComparable_comboBox);
            this.Controls.Add(this.currencyMain_comboBox);
            this.Name = "MainForm";
            this.Text = "Currency Converter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox currencyMain_comboBox;
        private System.Windows.Forms.ComboBox currencyComparable_comboBox;
        private System.Windows.Forms.Label rate_label;
        private System.Windows.Forms.DateTimePicker rate_dateTimePicker;
        private System.Windows.Forms.TextBox amount_textBox;
        private System.Windows.Forms.Label amount_label;
        private System.Windows.Forms.Label from_label;
        private System.Windows.Forms.Label to_label;
        private System.Windows.Forms.CheckBox date_checkBox;
  

    }
}

