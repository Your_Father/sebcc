﻿using CurrencyConverter.Data;
using CurrencyConverter.Logic.Utilities;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace CurrencyConverter
{
    public partial class MainForm : Form
    {

        IBaseCurrency _currentCurrency = new ECBCurrency();
        ICurrencyController _currencyController = new CurrencyController();
        ICurrencyValidator _currencyValidator = new CurrencyValidator();

        decimal _currentRate;
        string _previousCurrency;

        public MainForm()
        {
            InitializeComponent();
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            _currentCurrency = await _currencyController.LoadCurrencyAsync<ECBCurrency>(DateTime.Today);
            currencyMain_comboBox.Items.AddRange(_currencyController.ExtractAvaliableCurrincyNames(_currentCurrency));
            currencyComparable_comboBox.Items.AddRange(_currencyController.ExtractAvaliableCurrincyNames(_currentCurrency));
            currencyMain_comboBox.SelectedItem = "USD";
        }

        private void amount_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') ||
                (e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1);
        }

        private void sum_control_TextChanged(object sender, EventArgs e)
        {
            amount_label.Text = $"{_currencyController.CountAmount(amount_textBox.Text, _currentRate)} {currencyComparable_comboBox.Text}";
        }

        private void currencyMain_comboBox_Enter(object sender, EventArgs e)
        {
            object selectedItem = currencyMain_comboBox.SelectedItem;
            _previousCurrency = selectedItem != null ? selectedItem.ToString() : "USD";
        }

        private void date_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (date_checkBox.Checked)
            {
                rate_dateTimePicker.Visible = true;
            }
            else
            {
                rate_dateTimePicker.Visible = false;
                rate_dateTimePicker.Value = DateTime.Now;
            }
        }

        private async void currency_control_RelevantDataChanged(object sender, EventArgs e)
        {
            string selectedCurrency = currencyMain_comboBox.SelectedItem.ToString();
            _currentCurrency = await _currencyController.LoadCurrencyAsync<ECBCurrency>(rate_dateTimePicker.Value, selectedCurrency);
            UpdateRateUI();
        }

        private void currency_comboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateRateUI();
        }

        private void UpdateRateUI()
        {
            if (string.IsNullOrEmpty(currencyComparable_comboBox.Text))
            {
                return;
            }
            else if (_currencyValidator.RateIsValid(_currentCurrency, currencyComparable_comboBox.Text, out _currentRate))
            {
                rate_label.Text = $"Rate: {_currentRate}";
            }
            else
            {
                MessageBox.Show("Rate is not valid. Please, try another rate");
                currencyComparable_comboBox.SelectedItem = currencyMain_comboBox.SelectedItem;
            }
        }
    }
}
